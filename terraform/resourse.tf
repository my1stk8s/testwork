module "instance_1" {
  source = "./instance"
  name   = "dev"
  custom_subnet_id = yandex_vpc_subnet.subnet-tf-2.id
  instance_family_image = "ubuntu-2204-lts"
  zone_id = "ru-central1-b"
}
# создание сети
resource "yandex_vpc_network" "network-tf" {
  name = "network-tf"
}

resource "yandex_vpc_subnet" "subnet-tf-2" {
  name           = "subnet-tf-2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-tf.id
  v4_cidr_blocks = ["192.168.7.0/24"]
}

resource "local_file" "inventory" {
  filename = "../ansible/host"
  content = <<EOF

[dev]
dev ansible_host=${module.instance_1.external_ip_address_vm} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}
EOF
depends_on = [
    module.instance_1
  ]
}

resource "null_resource" "ConfigureAnsibleLabelVariable" {
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook  -i ../ansible/host ../ansible/deploy_dev.yml -vvv"
  }
  depends_on = [
    local_file.inventory
  ]
}




