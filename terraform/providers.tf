terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.98.0"
    }
  }
    backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "s3bucketacc"
    region     = "ru-central1"
    key        = "terraform.tfstate"
    
    skip_region_validation      = true
    skip_credentials_validation = true
  } 
}
# Variables

variable "token" {
  type = string
  description = "Yandex Cloud API key"
}

variable "cloud_id" {
  type = string
  description = "Yandex Cloud id"
}

variable "folder_id" {
  type = string
  description = "Yandex Cloud folder id"
}

# Provider

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
}


variable "ssh_key_private" {
  description = "ssh"
  type        = string
  default     = "~/.ssh/toster2"
}
