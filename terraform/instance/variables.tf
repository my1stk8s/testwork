variable "instance_family_image" {
  description = "Instance image"
  type        = string
}

variable "custom_subnet_id" {
  description = "VPC subnet network id"
  type        = string
}

variable "zone_id" {
  description = "Zone on yandex.cloud"
  type        = string
}

variable "name" {
  description = "Instance name"
  type        = string
}

variable "ssh_key_private" {
  description = "ssh"
  type        = string
  default     = "~/.ssh/toster2"
}


