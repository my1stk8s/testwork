terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.98.0"
    }
  }
}



resource "yandex_compute_instance" "machine" {
  name = "${var.name}"
  boot_disk {
    initialize_params {
      image_id = "fd830gae25ve4glajdsj"
      size = 10
    }
  }
  
  resources {
    core_fraction = 20
    cores  = 2
    memory = 2
  }
  scheduling_policy {
    preemptible = false
  }
  network_interface {
    subnet_id = var.custom_subnet_id
    nat = true
  }
  zone = var.zone_id
  metadata = {
    user-data = "${file("instance/meta.yml")}"
  }
}
